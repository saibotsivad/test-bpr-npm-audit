# demo of [bpr-npm-audit](https://github.com/saibotsivad/bpr-npm-audit)

If you haven't, go read the documentation of [bpr-npm-audit](https://github.com/saibotsivad/bpr-npm-audit).

Have a look at the [Pipelines YAML file](./bitbucket-pipelines.yml) file to see how the module is used.

Finally, have a look at the open pull request, to see what the report looks like.
